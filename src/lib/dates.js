const moment = require('moment');

module.exports = (tms, mins) => {
    let momentified = moment(tms * 1000).format('MM-DD YYYY @ h:mma');
    let splitMonth = momentified.split('-');
    splitMonth[0] = monthInLeters(splitMonth[0]);
    if (splitMonth[1][0] == '0') {
        splitMonth[1] = splitMonth[1].substr(1, splitMonth[1].length - 1);
    }
    let withMonth = splitMonth.join('-');
    return mins
        ? withMonth.replace('@', 'at')
        : withMonth.split('@')[0].trim();
};

function monthInLeters(m){
    return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][parseInt(m) - 1];
}
