import Vue from 'vue';
import Router from 'vue-router';
import Splash from '@/components/splash/Splash';
import Org from '@/components/org/Org';
import Building from '@/components/building/Building';
import Floor from '@/components/floor/Floor';
import Room from '@/components/room/Room';
import OBBldg from '@/components/onboard/Building';
import OBRoom from '@/components/onboard/Room';

Vue.use(Router);

export default new Router({
  scrollBehavior() {
     return { x: 0, y: 0 };
  },
  routes: [{
      path: '/',
      name: 'Splash',
      component: Splash
    },
    {
      path: '/organization',
      name: 'Org',
      component: Org
  }, {
      path: '/building',
      name: 'Building',
      component: Building
  }, {
      path: '/floor',
      name: 'Floor',
      component: Floor
  }, {
      path: '/room',
      name: 'Room',
      component: Room
  }, {
      path: '/newBuilding',
      name: 'OBBldg',
      component: OBBldg
  }, {
      path: '/newRoom',
      name: 'OBRoom',
      component: OBRoom
  }]
});
