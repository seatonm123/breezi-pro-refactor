
import axios from 'axios';

export default {

    requestAccess(json){
        let url = 'https://api-matt.breezi.net/requestAccess'
        return new Promise((resolve, reject) => {
            axios.post(url, json)
                .then(success => {
                    resolve('Success');
                }).catch(err => {
                    console.error(err);
                    reject({ERROR: err});
                });
        });
    },

    getInitialBundle(tkn){
      const url = 'https://api-matt.breezi.net/bundle';
      let headers = {
        headers: {
          Authorization: tkn,
          'Content-Type': 'application/json'
        }
      };
      return new Promise((resolve, reject) => {
          axios.get(url, headers)
            .then(bundle => {
                this.getTeam(tkn)
                    .then(team => {
                        bundle = bundle.data;
                        bundle.team = team.data;
                        resolve(bundle);
                    });
            }).catch(err => {
                console.log(err);
                reject({ERROR: err});
            });
      });
    },

    getTeam(tkn){
        const url = 'https://api-matt.breezi.net/team';
        let headers = {
          headers: {
            Authorization: tkn,
            'Content-Type': 'application/json'
          }
        };
        return new Promise(resolve => {
            axios.get(url, headers)
                .then(team => {
                    resolve(team);
                }).catch(err => {
                    resolve([]);
                });
        });
    },
    postBldg(bldg, tkn){
    let keys = Object.keys(bldg);
    return new Promise((resolve, reject) => {
      if (!tkn || tkn === undefined || tkn.length < 1) {
        reject(null);
      }
      // if (keys.every(hasProp)) {
        const url = 'https://api-matt.breezi.net/buildings';
        const headers = {
          Authorization: tkn,
          'Content-Type': 'application/json'
        };
        axios({
          method: 'POST',
          url: url,
          headers: headers,
          data: JSON.stringify(bldg)
        }).then(res => {
          bldg.admins = [],
          bldg.alerts = {
            co2: {
              bad: 0,
              crit: 0
            },
            days: {
              bad: 0,
              crit: 0
            },
            hhm: {
              bad: 0,
              crit: 0
            },
            iaq: {
              bad: 0,
              crit: 0
            },
            ttp: {
              bad: 0,
              crit: 0
            }
          };
          bldg.avg = {};
          bldg.overallAlerts = {bad: 0, crit: 0};
          resolve(bldg);
        }, err => {
          console.log(err);
          reject(null);
        });
    });
},

postNewRoom(room, tkn){
    const url = 'https://api-matt.breezi.net/rooms';
    return new Promise((resolve, reject) => {
      let headers = {
        'Content-Type': 'applciation/json',
        Authorization: tkn
      };
      axios({
        method: 'POST',
        url: url,
        headers: headers,
        data: room
      }).then(success => {
        resolve(true);
      }).catch(err => {
        reject(null);
      });
    })
  },

  changeFilter(vId, bId, tkn){
    const url = 'https://api-matt.breezi.net/changeFilter';
    return new Promise((resolve, reject) => {
      if (tkn === null) {
        reject("Your connection has been interrupted. Please log out and try again");
      } else {
        let headers = {
          Authorization: tkn,
          'Content-Type': 'application/json'
        };
        let body = JSON.stringify({
          airVentId: vId,
          buildingId: bId
        });
        axios({
          method: 'POST',
          url: url,
          headers: headers,
          data: body
        }).then(res => {
          resolve('success');
        }, err => {
          console.log(err);
          reject('The requested air filter could not be changed. Please log out and try again');
        })
      }
    });
  },

  deleteRoom(tkn, body){
    return new Promise((resolve, reject) => {
      let headers = {
        'Content-Type': 'application/json',
        Authorization: tkn
      };
      axios({
        method: 'DELETE',
        url: 'https://api-matt.breezi.net/rooms',
        headers: headers,
        data: body
      }).then(res => {
        resolve(`Success`);
      }).catch(err => {
        reject(err);
      })
    })
  },

  deleteBldg(tkn, bId){
    return new Promise((resolve, reject) => {
      let headers = {
        'Content-Type': 'application/json',
        Authorization: tkn
      };
      let url = `https://api-matt.breezi.net/buildings/${bId}`;
      axios({
        method: 'DELETE',
        url: url,
        headers: headers
      }).then(res => {
        resolve(res);
      }).catch(err => {
        reject(err);
      });
    });
  },

  postUser(body, tkn){
    const url = 'https://api-matt.breezi.net/team';
    return new Promise((resolve, reject) => {
      let headers = {
        'Content-Type': 'applciation/json',
        Authorization: tkn
      };
      axios({
        method: 'POST',
        url: url,
        headers: headers,
        data: body
      }).then(success => {
        resolve();
      }).catch(err => {
        console.log(err)
        reject();
      });
    })
  }

};
