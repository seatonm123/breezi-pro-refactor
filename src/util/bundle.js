
const moment = require('moment');

let Thresh = require('../lib/thresholds');
Thresh.co2.crit.high = Infinity;
const DateNorm = require('../lib/dates');

const rdgVals = ['days', 'ttp', 'hhm', 'iaq', 'co2'];

class RdgSchema {
    constructor(){

    }
    initAlerts(){
        const self = this;
        rdgVals.forEach(r => {
            self[r] = {
                bad: 0,
                crit: 0
            };
        });
    }
    initAvg(){
        const self = this;
        rdgVals.forEach(r => {
            self[r] = 0;
        });
    }
}

/*
    Alerts/avg work as follows:
        1. If no buildings exist, returns all-zero values for bldgAlerts, and 'no data' as val for averges
            'no data' will be used to identify inactive bldgs/floors/etc and input a BLUE icon/text value to differentiate between no alerts (green)
        2. 'Active' value is used to calculate averages, so if only 2/3 buildings have readings, the average can be calculated with this value instead of the full length of the bldg array
*/

function formatBundle(data){
    if (data === undefined || data.length === 0) {
        return {
            alerts: bldgAlerts,
            avg: 'no data',
            active: 0,
            bldgs: []
        };
    }
    let bldgAlerts = new RdgSchema();
    bldgAlerts.initAlerts();
    let bldgAvg = new RdgSchema();
    bldgAvg.initAvg();
    let bldgsObj = {
        alerts: bldgAlerts,
        avg: bldgAvg,
        active: 0,
        bldgs: [],
        overallAlerts: {bad: 0, crit: 0}
    };

    bldgsObj.bldgs = data.map(bldg => {
        bldg = compileBldgRdgs(bldg);
        if (bldg.active) {
            bldgsObj.active++;
        }
        return bldg;
    });

    let activeBldgs = bldgsObj.bldgs.filter(b => b.active);
    bldgsObj.avg = getAvg(activeBldgs.map(b => b.avg));
    bldgsObj.bldgs.forEach(b => {
        bldgsObj.overallAlerts.bad += b.overallAlerts.bad;
        bldgsObj.overallAlerts.crit += b.overallAlerts.crit;
        Object.keys(b.alerts).forEach(a => {
            bldgsObj.alerts[a].bad += b.alerts[a].bad;
            bldgsObj.alerts[a].crit += b.alerts[a].crit;
        });
    });

    return bldgsObj;

}

// REMEMBER: 'ACTIVE' PROP

function compileBldgRdgs(b){

    let bAlerts = new RdgSchema();
    bAlerts.initAlerts();
    let bAvg = new RdgSchema();
    bAvg.initAvg();

    let bObj = {
        alerts: bAlerts,
        avg: bAvg,
        active: false,
        floors: [],
        info: {},
        overallAlerts: {bad: 0, crit: 0}
    };

    Object.keys(b).forEach(k => {
        if (k !== 'admins' && k !== 'floors' && k !== 'org') {
            bObj.info[k] = b[k];
        }
    });

    if (b.floors.every(f => !f.active)) {
        bObj.floors = b.floors;
        return bObj;
    }

    bObj.active = true;

    bObj.floors = b.floors.map(flr => {
        flr = compileFloorRdgs(flr);
        return flr;
    });

    let activeFloors = bObj.floors.filter(f => f.active);
    bObj.avg = getAvg(activeFloors.map(f => f.avg));
    bObj.floors.forEach(f => {
        bObj.overallAlerts.bad += f.overallAlerts.bad;
        bObj.overallAlerts.crit += f.overallAlerts.crit;
        Object.keys(f.alerts).forEach(a => {
            bObj.alerts[a].bad += f.alerts[a].bad;
            bObj.alerts[a].crit += f.alerts[a].crit;
        });
    });

    return bObj;

}

function compileFloorRdgs(f){

    let fAlerts = new RdgSchema();
    fAlerts.initAlerts();
    let fAvg = new RdgSchema();
    fAvg.initAvg();

    let fObj = {
        alerts: fAlerts,
        avg: fAvg,
        active: false,
        rooms: [],
        info: {},
        overallAlerts: {bad: 0, crit: 0}
    };

    Object.keys(f).forEach(k => {
        if (k !== 'rooms') {
            fObj.info[k] = f[k];
        }
    });

    if (!f.active || !f.rooms) {
        return fObj;
    }

    fObj.active = true;

    fObj.rooms = f.rooms.map(room => {
        return compileRoomRdgs(room);
    });

    let activeRooms = fObj.rooms.filter(r => r.active);
    fObj.avg = getAvg(activeRooms.map(r => r.avg));
    fObj.rooms.forEach(r => {
        fObj.overallAlerts.bad += r.overallAlerts.bad;
        fObj.overallAlerts.crit += r.overallAlerts.crit;
        Object.keys(r.alerts).forEach(a => {
            fObj.alerts[a].bad += r.alerts[a].bad;
            fObj.alerts[a].crit += r.alerts[a].crit;
        });
    });

    return fObj;

}

function compileRoomRdgs(r){

    let rAvg = new RdgSchema();
    rAvg.initAvg();

    let rObj = {
        alerts: {},
        avg: rAvg,
        active: false,
        info: {},
        changed: '',
        hasDap: false,
        overallAlerts: {bad: 0, crit: 0}
    };

    Object.keys(r).forEach(k => {
        if (!['ac', 'aq', 'configuration', 'floorIndex', 'latestDap', 'latestMeasPacket', 'size'].includes(k)) {
            rObj.info[k] = r[k];
        }
    });

    let currentFilter = r.airFilters.find(f => !f.toTimestamp);

    if (r.active === false || r.latestMeasPacket === null || currentFilter === undefined) {
        let alerts = new RdgSchema();
        alerts.initAlerts();
        rObj.alerts = alerts;
        return rObj;
    }

    rObj.active = true;

    if (r.latestDap && r.latestDap !== null) {
        rObj.hasDap = true;
        rObj.avg.days = r.latestDap.values.lifeExpectancy;
    } else {
        rObj.days = '-';
    }

    Object.keys(rObj.avg).forEach(m => {
        const dat = r.latestMeasPacket.dat;
        if (m !== 'days') {
            if (dat[m]) {
              console.log({rdg: m, data: dat[m]})
                rObj.avg[m] = Math.round(dat[m][dat[m].length - 1]);
            } else {
                rObj.avg[m] = '-';
            }
        }
    });

    rObj.lastUpdated = DateNorm((r.latestMeasPacket.tms + (r.latestMeasPacket.ctr * r.latestMeasPacket.per)), true);
    rObj.lastChanged = DateNorm(currentFilter.fromTimestamp);

    rObj.alerts = parseThresh(rObj.avg);

    Object.keys(rObj.alerts).forEach(a => {
        rObj.overallAlerts.bad += rObj.alerts[a].bad;
        rObj.overallAlerts.crit += rObj.alerts[a].crit;
    });

    return rObj;
}

function parseThresh(avg){
    let template = new RdgSchema();
    template.initAlerts();
    Object.keys(avg).forEach(k => {
        if ((avg[k] > Thresh[k].bad.low && avg[k] < Thresh[k].bad.high) || (Thresh[k].bad2 && avg[k] > Thresh[k].bad2.low && avg[k] < Thresh[k].bad2.high)) {
            template[k].bad++;
        }
        if ((avg[k] > Thresh[k].crit.low && avg[k] < Thresh[k].crit.high) || (Thresh[k].crit2 && avg[k] > Thresh[k].crit2.low && avg[k] < Thresh[k].crit2.high)) {
            template[k].crit++;
        }
    });
    return template;
}

function getAvg(avgs){
    let template = {
        days: {total: 0, count: 0},
        ttp: {total: 0, count: 0},
        hhm: {total: 0, count: 0},
        iaq: {total: 0, count: 0},
        co2: {total: 0, count: 0}
    };
    avgs.forEach(a => {
        Object.keys(a).forEach(k => {
            if (a[k] !== '-') {
                template[k].count++;
                template[k].total += a[k];
            }
        });
    });
    Object.keys(template).forEach(t => {
        if (template[t].count === 0) {
            template[t] = '-';
        } else {
            template[t] = Math.round(template[t].total/template[t].count);
        }
    });
    return template;
}

export default formatBundle;
