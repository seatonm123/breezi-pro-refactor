import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import Rest from './util/rest';
import formatBundle from './util/bundle';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    bundle: {},
    team: [],
    org: {},
    error: {}
  },
  getters: {
    bundle: state => state.bundle,
    hasData: state => !!state.bundle.buildings
  },
  mutations: {
    setBundle(state, bundle){
      state.bundle = formatBundle(bundle);
    },
    bundleErr(state, err){
      state.error = err;
    }
  },
  actions: {
    async getBundle(context, tkn){
      try {
        const bundle = await Rest.getInitialBundle(tkn);
        context.commit('setBundle', bundle);
      } catch(err) {
        context.commit('bundleErr', err);
      }
    }
  },
  modules: {}
})
