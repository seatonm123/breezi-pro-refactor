// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Highcharts from 'highcharts'
import boost from 'highcharts/modules/boost'
import store from './store';
import VueHighCharts from 'vue-highcharts'
import VueCookie from 'vue-cookies'
Vue.use(VueCookie);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
Vue.use(VueHighCharts);
